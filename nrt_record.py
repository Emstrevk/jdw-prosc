from __future__ import annotations
from typing import Any


# TODO: This is the newest TODO from June 2021
#DONE 1. Synth rows need to be reworked to contain ALL args, like s_new in main
#OK 2. Once the synth rows are streamlined that way, we can use them for samples as well
#OK 3. build_nrt's only difference for sample is that it must pre-contain the buffer read rows
#DONE   - So one might add an optional messages arg where you can pass along the BufferData.to_nrt rows 
# 4. We also need effect messages and group separation 
#DONE 5. Note: For better genericness we should send the complete array of row-as-osc to the build function. 
#   this way we can preconstruct osc the same wway we do in main. It also removes the need for "first_rows"


def get_osc_arg(osc_msg: list[any], arg_name: str) -> any:
    
    index = 0
    for arg in osc_msg:
        if arg == arg_name:
            return osc_msg[index + 1]
        
        index += 1
    
    return None 

# Returns the scd code needed to create a wav file from the note set
def build_nrt_script(osc_messages: list[list[any]], nrt_messages: list[list[any]], bpm: int, wav_file_name: str) -> str:
    
    rows = []


    # See main.py - should correspond with standard 
    # effects/outputs groups 
    rows.append(str([0.0, ["/g_new", 10]]))
    rows.append(str([0.0, ["/g_new", 20, 1, 10]]))


    rows += [str(msg) for msg in nrt_messages]

    time: float = 0.0

    for msg in osc_messages:

        # s_new osc args have id at index 1
        if len(msg) < 1:
            print("ERROR: Malformed message: " + str(msg))
        else:
            msg_id = msg[1]

            msg.insert(0, "/s_new")
            timed_msg = [time, msg]

            time += get_osc_arg(msg, "time")

            gate_off = [time + get_osc_arg(msg, "sus"), ["/n_set", msg_id, "gate", 0.0]]

            rows.append(str(timed_msg))
            rows.append(str(gate_off))
    
    with open ("scd_template/nrt_record.scd", "r") as final_template:
        
        filled = final_template.read()

        for (src, tgt) in [("{SCORE_ROWS}", ",\n    ".join(rows)), ("{FILE_NAME}", wav_file_name), ("{BPM}", str(bpm)), ("{END_TIME}", str(time))]:
            filled = filled.replace(src,tgt)

        return filled







