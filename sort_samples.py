# TODO: attempt to streamline sample naming

import samples
import os
from samples import BufferData, BufferSample, get_buffer_data

class SampleFamilyDict:
    def __init__(self):
        self.packs: dict[str, dict[str, list[BufferSample]]] = {}

    def add(self, pack: str, family: str, sample: BufferSample):
        if pack not in self.packs:
            self.packs[pack] = {}
        if family not in self.packs[pack]:
            self.packs[pack][family] = []
        self.packs[pack][family].append(sample)

    def get(self, pack: str, family: str, index: int):
        if pack in self.packs and family in self.packs[pack]:
            return self.packs[pack][family][index % len(self.packs[pack][family]) - 1]
        else:
            return None

    def debug(self):
        for key in self.packs:
            for family in self.packs[key]:
                print(key, family, [bs.file_path for bs in self.packs[key][family]])

def get_sorted(buffer_data: BufferData) -> SampleFamilyDict:
    
    buffer_data = samples.get_buffer_data()

    def matches(name: str, tags: dict[str, list]) -> bool:
        return any(tag in name.lower() for tag in tags["yes"]) and not any(tag in name.lower() for tag in tags["no"])

    bass_tags = {"yes": ["bass", "drum", "kick"], "no": ["crash"]}
    hat_tags = {"yes": ["hat", "stick"], "no": []}
    shake_tags = {"yes": ["maraca", "shake", "tambou", "tamb", "casta"], "no": []}
    tom_tags = {"yes": ["tom", "bongo", "conga", "block"], "no": []}
    snare_tags = {"yes": ["snare", "clap"], "no": []}
    cymbal_tags = {"yes": ["cymbal", "ride", "crash"], "no": []}
    bell_tags = {"yes": ["bell"], "no": []}

    sfd = SampleFamilyDict()
    for key in buffer_data.samples:

        pack = buffer_data.samples[key]
        for sample in pack.samples:
            name = sample.file_path
            if matches(name, hat_tags):
                sfd.add(key, "hh", sample)
            elif matches(name, bass_tags):
                sfd.add(key, "bd", sample)
            elif matches(name, shake_tags):
                sfd.add(key, "sh", sample)
            elif matches(name, tom_tags):
                sfd.add(key, "to", sample)
            elif matches(name, snare_tags):
                sfd.add(key, "sn", sample)
            elif matches(name, cymbal_tags):
                sfd.add(key, "cy", sample)
            elif matches(name, bell_tags):
                sfd.add(key, "be", sample)
            else:
                sfd.add(key, "mi", sample)

    return sfd

if __name__ == "__main__":
    bufferd = get_buffer_data()
    sfd = get_sorted(bufferd)
    for i in range(0,20):

        assert sfd.get("DR660", "hh", i).file_path != ""