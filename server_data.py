from datetime import timedelta, datetime

ADD_ACTION_HEAD_OF_GROUP = 0
OUTPUT_GROUP = 10

class RunningNote:
    def __init__(self, nodeId: int, original_msg: dict[any, any], auto_gate = True):
        self.nodeId = nodeId
        self.original_msg = original_msg
        self.start_time = datetime.now()
        self.auto_gate = auto_gate

    def is_pressed(self, time: datetime) -> bool:
        sus = self.original_msg["args"]["sus"] if "sus" in self.original_msg["args"] else 0.0
        return time < (self.start_time + timedelta(seconds=sus))

    def is_active(self, time: datetime) -> bool:
        sus = self.original_msg["args"]["sus"] if "sus" in self.original_msg["args"] else 0.0
        rel = self.original_msg["args"]["relT"] if "relT" in self.original_msg["args"] else 0.0
        return time < (self.start_time + timedelta(seconds=sus + rel))

    def is_safe_to_remove(self, time: datetime) -> bool:
        sus = self.original_msg["args"]["sus"] if "sus" in self.original_msg["args"] else 0.0
        rel = self.original_msg["args"]["relT"] if "relT" in self.original_msg["args"] else 0.0
        return time >= (self.start_time + timedelta(seconds=(sus + rel) * 4))

    def get_amp(self) -> float:
        return self.original_msg["args"]["amp"] if "amp" in self.original_msg["args"] else 0.0

    def to_sample_OSC(self, buffer_sorted, buffer_data) -> list[any]:

        tone = int(self.original_msg["index"])

        # Make sure not to play silent "notes"
        if self.get_amp() > 0.0:

            # Expected s_new osc arg format
            # see sampler.scd for synth definition, note also the key "buf" arg
            family = self.original_msg["family"]
            sample = buffer_sorted.get(self.original_msg["target"], family, tone) if family != "" else buffer_data.get_sample(self.original_msg["target"], tone)

            if sample:
                args = [
                    "sampler",
                    self.nodeId, 
                    ADD_ACTION_HEAD_OF_GROUP, 
                    OUTPUT_GROUP, 
                    "buf", 
                    sample.buffer_number
                ]

                # Pad em the same as regular s_new. This is a bit wonky since it ends up passing "tone" along as well 
                # but that will have to do for now
                for key in self.original_msg["args"]:
                    args.append(key)
                    args.append(self.original_msg["args"][key])

                return args

        return [] 

    def _apply_relative_args(self, relative_args_map: dict[str, float] = {}) -> dict[str, float]:
        base_args = self.original_msg["args"]
        new_args = {}
        for arg in base_args:
            mod = relative_args_map[arg] if arg in relative_args_map else 0.0
            new_args[arg] = base_args[arg] + mod
        return new_args

    # Situational "override all args but still apply relative if necessary"
    def to_override_mod_OSC(self, base_args = {}, relative_args = {}):
        new_args = {}
        for arg in base_args:
            mod = relative_args[arg] if arg in relative_args else 0.0
            new_args[arg] = base_args[arg] + mod

        osc_msg = [self.nodeId]
        for arg in new_args:
            osc_msg.append(arg)
            osc_msg.append(new_args[arg])

        print(osc_msg)
        return osc_msg 


    def to_mod_OSC(self, relative_args = {}) -> list[any]:
        assigned_id = self.nodeId
        osc_msg = [assigned_id]
        full_args = self._apply_relative_args(relative_args)
        for arg in full_args:
            osc_msg.append(arg)
            osc_msg.append(full_args[arg])
        return osc_msg

    def to_OSC(self, relative_args = {}) -> list[any]:

        # Expected s_new osc arg format
        args = [
            self.original_msg["target"], # e.g. "stockSaw"
            self.nodeId,
            ADD_ACTION_HEAD_OF_GROUP, # Add action: head of target id group
            OUTPUT_GROUP # target id (implicitly: group)
        ]

        # Export args dict to OSC format of ["key", value, "key", value]
        full_args = self._apply_relative_args(relative_args)
        for key in full_args:
            args.append(key)
            args.append(full_args[key])      

        return args

    def mod(self, relative_args: dict, relative = True):
        for key in relative_args:
            if key in self.original_msg["args"] and relative:
                self.original_msg["args"][key] += relative_args[key]
            else:
                self.original_msg["args"][key] = relative_args[key]
