from samples import get_buffer_data, BufferData
import os 


bd: BufferData = get_buffer_data()

for sample_pack in bd.samples:
    print("#########", sample_pack)
    number = 0
    for sample in bd.samples[sample_pack].samples:
        print("    ", str(number), os.path.basename(sample.file_path))
        number += 1