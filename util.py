import os 

# SuperCollider uses ids to keep track of created synths
# These can be auto-assigned by passing -1, but for extra control
# we use an auto-incrementer instead
class SynthIdIncrementer:
    def __init__(self):
        self.index = 1000
        self.max = 38000 # Arbitrary. Typically old ids can be reused after an awful long time. 
    def next(self):
        self.index += 1
        if self.index > self.max:
            self.index = 0
        return self.index


# Get a set of dicts decribing the | | separated pass-in args of each scd synth 
def get_synth_descriptions() -> dict[dict[any, any]]:
    synths = {}
    for scd in os.listdir("synths"):
        with open("synths/" + scd) as data:
            contents = data.read() 
            if contents:
                arg_split = contents.split("|")
                if len(arg_split) > 2:
                    arg_chunk = "".join(arg_split[1:2])
                    synths[scd] = {}
                    for arg in arg_chunk.split(","):
                        keyval_split = arg.split("=")
                        if len(keyval_split) > 1:

                            def_val = float(keyval_split[1].strip())

                            synths[scd][keyval_split[0].strip()] = def_val
                        else:
                            synths[scd][arg.strip()] = 0.0
    
    return synths      