import zmq
import json
import time
import sys

ctx = zmq.Context()
socket = ctx.socket(zmq.PUSH)
socket.connect("tcp://localhost:5559")

def play(freq, sus):
    payload = {"target": "stockSaw", "external_id": "saw101", "source": "test", "args": {"freq": freq, "amp": 1.0, "sus": sus}}
    socket.send_string("JDW.PLAY.NOTE::" + json.dumps(payload))

time.sleep(0.1)
play(261.63, 0.125)
time.sleep(0.1)
play(293.66, 0.125)
time.sleep(0.125)
play(392.00, 0.25)


