import liblo # For calling client scd load method
from subprocess import PIPE, Popen # For running the supercollider process
from os import listdir, path # For reading the syndef files off disc

SCLANG_EXEC = "sclang" # Command as used when running in shell
CLIENT_PORT = 13336 # Arbitrary port sure not to be taken
SERVER_PORT = 13337 # Configurable in start_server.scd
SERVER_IP = "127.0.0.1" # Typically localhost unless you're trying to be a hero of some kind

# Convenience class for keeping track of the OSC servers used to communicate with SuperCollider
class SuperCollider:
    def __init__(self):        
        # Launch supercollider
        # Note that we run sclang, which in turn runs a startup file 
        # that in turn boots a server instance
        self.sclang = Popen(
            [SCLANG_EXEC, "start_server.scd", "-u", str(CLIENT_PORT)],
            stdin=PIPE
        )

        # Establish connection
        self.client_address = liblo.Address(SERVER_IP, CLIENT_PORT)
        self.server_address = liblo.Address(SERVER_IP, SERVER_PORT)

    ### Basic OSC sending functions: server == scsynth and client == sclang
    def send_to_server(self, key, args):
        liblo.send(self.server_address, key, *args)
    def send_to_client(self, key, args):
        liblo.send(self.client_address, key, *args)

    def kill(self):
        self.sclang.kill()

    # Runs all scripts from the given folder. 
    # Convenient way to run 4sclang code from prosc.
    # See definition of function in start_server.scd
    def load_custom(self, in_folder: str):
        for script in listdir(in_folder):
            if (".scd" in script):
                self.send_to_client("/customscript", [in_folder + "/" + script, path.basename(script)])
