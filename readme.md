# Python Supercollider/OSC Translator
Small microservice application providing an integration layer between other applications and Supercollider by routing ZeroMQ messages to OSC.
	
 * Manages an instance of the supercollider client and server.
 * Automatically compiles and loads user-created scd scripts on boot (examples included for synths)
 * Provides example of buffer-to-synth based sample playing
 * Implements a set of common use function message scenarios, e.g. s_new or play_sample

### Why
Supercollider is a bit cryptic to integrate with and I can never seem to wrap
	my head around any of the numerous supercollider libraries for non-sclang
	languages. This application provides a simple POC integration service
	that can be reused by any project in any language.
The project came about when I wanted to harness the synth definition and audio output
	capabilities of SuperCollider without necessarily delving too deep into its interpreted
	language. As such the core functionality is to let the user define synths and play them
	using an s_new abstraction. 

### How
Main script prosc.py starts an instance of both the scsynth server and sclang client that 
	make up the SuperCollider application. By providing a startup .scd script, it configures 
	and extends the server and client with needed integration features. Via the jdw-broker application
	you can then send ZeroMQ messages to trigger common supercollider operations, such as the s_new OSC message.
	Via templating, it is also possible to interact directly with the sclang client by sending full scd scripts. 

### Prerequisites
Only tested with JACK AUDIO backend on arch linux. If no audio you might 
	have to tweak start_server.scd with more options.
SCLang version at time of creation: 3.11.1 

### Running

python3 -m venv env # Establish a venv
python main.py

### Usage

#### Folders
- sample_packs: place wav files in your own subfolders here to make samples available for playback via trigger_sample
	- e.g. creating sample_packs/mypack/cymbal.wav will make that wav available by posting a sample play message, stating "mypack" as instrument and the
		ordinal number (sorted by name, alphabetic) of the sample file as tone/freq. 
- synths: place synth-defining .scd files here to make them available via the s_new endpoint using the provided synth name
	- See blipp.scd for format outline 

#### TODO: Message Format and API 
- For now, refer to the message processing outline in main.py as well as e.g. jdw-sequencer's creation of such messages 