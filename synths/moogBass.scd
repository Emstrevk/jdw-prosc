/*
Mitchell Sigman (2011) Steal this Sound. Milwaukee, WI: Hal Leonard Books
pp. 18-19
Adapted for SuperCollider and elaborated by Nick Collins
http://www.sussex.ac.uk/Users/nc81/index.html
under GNU GPL 3 as per SuperCollider license
Minor SynthDef modifications by Bruno Ruviaro, June 2015.
*/

SynthDef("moogBass", {
	|bus = 0, pan = 0, freq = 440, amp = 0.1, gate = 1, lpf = 8000, gain = 3.0, prt = 0.01, attT = 0.001, decT = 0.3, 
	sus = 0.9, relT = 0.2, fx = 0.0|

	var osc, filter, env, filterenv, snd, chorusfx;

	osc = Mix(VarSaw.ar(
		freq: freq.lag(prt) * [1.0, 1.001, 2.0],
		iphase: Rand(0.0,1.0) ! 3,
		width: Rand(0.5,0.75) ! 3,
		mul: 0.5));

	filterenv = EnvGen.ar(
		envelope: Env.asr(0.2, 1, 0.2),
		gate: gate);

	filter =  MoogFF.ar(
		in: osc,
		freq: lpf * (1.0 + (0.5 * filterenv)),
		gain: gain);

	env = EnvGen.ar(
		envelope: Env.adsr(attT, 0.3, 0.9, 0.2, amp),
		gate: gate,
		doneAction: Done.freeSelf);

	snd = (0.7 * filter + (0.3 * filter.distort)) * env;

	chorusfx = Mix.fill(7, {

		var maxdelaytime = rrand(0.005, 0.02);
		DelayC.ar(
			in: snd,
			maxdelaytime: maxdelaytime,
			delaytime: LFNoise1.kr(
				freq: Rand(4.5, 10.5),
				mul: 0.25 * maxdelaytime,
				add: 0.75 * maxdelaytime)
		)
	});

	snd = snd + (chorusfx * fx);
	Out.ar(bus, Pan2.ar(snd, pan));

},
metadata: (
	credit: "Nick Collins",
	category: \bass,
	tags: [\pitched, \bass]
)
).writeDefFile("synthdefs");
