SynthDef("simpleKick", {arg bus = 0, amp = 0.3, sinFreq = 60, glissf = 0.9, attT = 0.01, relT = 0.45, pan = 0;
	var gliss = XLine.kr(sinFreq, sinFreq*glissf, relT);
	var snd = SinOsc.ar(gliss);
	var env = Env.perc(attT, relT).kr(doneAction: 2);
	snd = snd * env * amp;
	Out.ar(bus, Pan2.ar(snd, pan));
},
metadata: (
	credit: "Bruno Tucunduva Ruviaro",
	category: \drums,
	tags: [\percussion, \kick]
)
).writeDefFile("synthdefs");