SynthDef.new(\effect_HPF,
{|in, out, hpf = 500, hpr = 0.04|
var osc;
osc = In.ar(in, 2);
osc = RHPF.ar(osc, hpf, hpr);
Out.ar(out, osc)}).writeDefFile("synthdefs");