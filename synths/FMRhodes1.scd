  /*
Retrieved from: http://sccode.org/1-522

FM Rhodes Synthesizer

by Nathan Ho (snappizz)

Native SuperCollider port of STK's Rhodey. This should be preferred over the StkInst version because:

- It uses much less CPU.
- It is easier to modify.
- It doesn't require sc3-plugins or a correct setting of StkGlobals.
- It's beginner-friendly because it uses only basic UGens: SinOsc, EnvGen, Mix, Pan2, Out.

Modified by Josh Mitchell and Bruno Ruviaro in July 2019.

*/

SynthDef(\FMRhodes1, {
    arg
    // standard meanings
    bus = 0, freq = 0, gate = 1, pan = 0, amp = 1.0, gain = 0.3, attT = 0.001, relT = 1.0, lfoS = 8.8, inputLevel = 0.2,
    // all of these range from 0 to 1
    modIndex = 0.2, mix = 0.2, lfoD = 0.1;

    var env1, env2, env3, env4;
    var osc1, osc2, osc3, osc4, snd;
    amp = amp * gain;

    env1 = Env.perc(attT, relT * 1.25, inputLevel, curve: \lin).kr;
    env2 = Env.perc(attT, relT, inputLevel, curve: \lin).kr;
    env3 = Env.perc(attT, relT * 1.5, inputLevel, curve: \lin).kr;
    env4 = Env.perc(attT, relT * 1.5, inputLevel, curve: \lin).kr;

    osc4 = SinOsc.ar(freq) * 6.7341546494171 * modIndex * env4;
    osc3 = SinOsc.ar(freq * 2, osc4) * env3;
    osc2 = SinOsc.ar(freq * 30) * 0.683729941 * env2;
    osc1 = SinOsc.ar(freq * 2, osc2) * env1;
    snd = Mix((osc3 * (1 - mix)) + (osc1 * mix));
  	snd = snd * (SinOsc.ar(lfoS).range((1 - lfoD), 1));

    snd = snd * Env.asr(0, 1, relT).kr(gate: gate, doneAction: Done.freeSelf);
    snd = Pan2.ar(snd, pan, amp);

    Out.ar(bus, snd);
},
metadata: (
	credit: "Nathan Ho",
	category: \keyboards,
	tags: [\pitched, \piano, \fm]
)
).writeDefFile("synthdefs");
