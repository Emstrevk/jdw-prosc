SynthDef.new(\effect_reverb,
{|out = 0, room = 0.5, mix = 0.5, in|
var osc;
osc = In.ar(in, 2);
osc = FreeVerb.ar(osc, mix, room);
Out.ar(out, osc)}).writeDefFile("synthdefs");