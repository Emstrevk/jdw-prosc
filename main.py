import sys # For handling exit etc.
import time # For sleep()
import threading # Introduced to thread the synth garbage collector
from typing import Dict, Any, List

from samples import recompile, BufferData
from nrt_record import build_nrt_script # Non-realtime recording
from supercollider import SuperCollider # Communication with sclang and scsynth
from util import SynthIdIncrementer # Basic auto-increment integer
import zmq # Messaging with jdw-router
import os
import json # Message decoding 
import numpy as np
from datetime import datetime, timedelta, timezone # ms-perfect timed messages 
from sort_samples import SampleFamilyDict, get_sorted
from pretty_midi import note_number_to_hz # Sorta scope-creep, but there are current issues with doing this in Rust 
import re # For regex search of external ids 
import subprocess # for welcoming message
from server_data import RunningNote
import time
from dateutil.parser import parse as dateparse

SERVER_BOOT_WAIT_SECONDS = 3 # Arbitrary 
ADD_ACTION_HEAD_OF_GROUP = 0

### Setup

# ENV var required for NRT functionality.
# On bootup, the NRT server will automatically read any .scsyndef files 
# available in the variable-provided path 
synths_dir = os.path.abspath("synthdefs")                                                                                                                         
os.environ["SC_SYNTHDEF_PATH"] = synths_dir                                                                                                                          
print("Set synths dir to " + synths_dir)

# Init the actual, "daemon", server process
incrementer = SynthIdIncrementer()
sc = SuperCollider()

# Wait for server to become ready. Should be an async check instead but welly well.
time.sleep(SERVER_BOOT_WAIT_SECONDS)

### Configuration

# Print all received OSC output in stdout
#sc.send_to_server("/dumpOSC", [1])

# Ignore errors (useful when not interested in n_set non-issues)
sc.send_to_server("/error", [0])

# Create the needed groups for effects and output
# Groups are used to define order, in this case to ensure all 
# outputs are always added -before- all effects, since effects 
# can only apply to ugens "further back"
output_group = 10
effect_group = 20
sc.send_to_server("/g_new", [output_group]) 
sc.send_to_server("/g_new", [effect_group, 1, output_group]) 

# Create all needed custom scripts for the buffer-based sample synths
buffer_data: BufferData = recompile()
buffer_sorted: SampleFamilyDict = get_sorted(buffer_data)

# Compile all scd scripts in /synths into .scsyndef format and load them on the server
sc.load_custom("synths")
sc.send_to_server("/d_loadDir", ["synthdefs"])

sc.load_custom("custom_scripts")

# Keep track of added effect synths, typically with attributes 
# "bus", "nodeId" and "target"
# This is used to determine wether an effect message should 
# create a new effect or modify parameters of an existing one in the same bus
effects: list[dict[str, any]] = []
effect_messages = [] # TODO: Saves all messages related to effects to use for NRT recording. Clumsy - rework when refactoring effects.

# Map of <external_id, RunningNote>
# Useful when external callers want to keep track of added synths, e.g. for n_set
external_id_map = {}

# Map of <external_id, args>
# The args are added to the args of that id, producing a relative offset for all calls to new notes or n_set 
# Long term plan is some sort of regex setup, but for now the id is literal
# Note that pycompose uses "sequencer_tag" as external id 
# TODO: Regex (see impl in RunningNote)
# TODO: to_sample_osc
# TODO: Not applicable for NRT? 
relative_args_map: dict[str, dict[str, float]] = {}
def set_relative_args(id, rel_args):
    global relative_args_map
    relative_args_map[id] = rel_args
def get_relative_args(id):
    global relative_args_map
    # NOTE: Currently using simple wildcard match instead of regex
    matching_ids = [hit for hit in relative_args_map if id in hit]
    return relative_args_map[id] if id in matching_ids else {}

def send_off_notes():
    thread_safe_map = external_id_map # TODO: Remnants of thread
    removable = []
    for key in thread_safe_map:
        note: RunningNote = thread_safe_map[key]
        if not note.is_pressed(datetime.now()) and note.auto_gate:
            sc.send_to_server("/n_set", [note.nodeId, "gate", 0.0])
        if note.is_safe_to_remove(datetime.now()) and note.auto_gate:
            sc.send_to_server("/n_free", [note.nodeId])
            removable.append(key)
    
    for key in removable:
        del external_id_map[key]

def get_external_ids(regex: str) -> list[str]:
    # Remember: wildcards are ".*", not plain "*"
    r = re.compile(regex)
    return list(filter(r.match, external_id_map.keys()))

def update():
    global buffer_sorted
    global buffer_data

    print("######## UPDATING SYNTHS")
    buffer_data = recompile()
    buffer_sorted = get_sorted(buffer_data)
    sc.load_custom("synths")
    time.sleep(0.01) # This needs some kind of reactivity
    sc.send_to_server("/d_loadDir", ["synthdefs"])
    sc.load_custom("custom_scripts")

    print("### DONE ")

    return "OK"                    

def kill_effects():
    global effects 
    global effect_messages

    print("############# KILLING ALL EFFECTS")

    local_effects = [effect["nodeId"] for effect in effects]
    effects = []
    effect_messages = []

    for effect in local_effects:
        sc.send_to_server("/n_free", [effect])
        print("#### Removing effect id", effect)
    
    print("###### EFFECTS REMOVED")

# TODO: Bus handling is a bit hacky (parsing args), but hard to do better ....  
def add_effect(data_arr: list[dict[any, any]]):
    global effects 
    
    for data in data_arr:

        provided_args = []

        # Export args dict to OSC format of ["key", value, "key", value]
        for key in data["args"]:
            provided_args.append(key)
            provided_args.append(data["args"][key])

        # If same external id already exists, n_set instead of creating new 
        if data["external_id"] in [effect["external_id"] for effect in effects]:
            existing_id = [effect["nodeId"] for effect in effects if effect["external_id"] == data["external_id"]][0]
            args = [existing_id] + provided_args
            sc.send_to_server("/n_set", args)
            effect_messages.append(["/n_set"] + args)
        else:

            new_id = incrementer.next()
            data["nodeId"] = new_id
            args = [data["target"], new_id, ADD_ACTION_HEAD_OF_GROUP, effect_group] + provided_args

            sc.send_to_server("/s_new", args)
            effect_messages.append(["/s_new"] + args)

            # Save the new effect for later reference
            effects.append(data)

    print("##### ACTIVE EFFECT IDS", [effect["nodeId"] for effect in effects])


# Send an s_new message to the server without any predetermined sustain time
# With external_id, the caller can then call modify_note() for any other operations
# Message format: {"target": str, "external_id": str, "args": {str, float}}
def new_note(note_message: dict[any,any]):

    # TODO: Ugly hack due to lacking tone support in Rust
    if "tone" in note_message["args"]:
        note_message["args"]["freq"] = note_number_to_hz(note_message["args"]["tone"])

    note = RunningNote(incrementer.next(), note_message, False)

    external_id = note_message["external_id"]
    external_id_map[external_id] = note
    print("Triggering new note with id", external_id, "and osc", note.to_OSC(get_relative_args(external_id)))
    sc.send_to_server("/s_new", note.to_OSC(get_relative_args(external_id)))

# Plain setter for an active note via osc
def n_set(external_id, args):
    print("Calling n_set with id", external_id, "and args", args)
    if external_id in external_id_map:
        note: RunningNote = external_id_map[external_id]
        #if note.is_active(datetime.now()):
        mod = note.to_override_mod_OSC(args, get_relative_args(external_id))
        print("Setting", mod)
        sc.send_to_server("/n_set", mod)

# Add permanent relative modification to matching external ids  
# Message format: {"external_id": str, "args": {str, float}}
def modify_note(mod_message: dict[any,any]):

    set_relative_args(mod_message["external_id"], mod_message["args"])
    # Check if any running note matches wildcard id
    if len([message for message in external_id_map if mod_message["external_id"] in message]) > 0:
        # Mod all active notes with the current relative args
        for key in external_id_map:    
            existing_note: RunningNote = external_id_map[key]
            if existing_note.is_active(datetime.now()):
                sc.send_to_server("/n_set", existing_note.to_mod_OSC(get_relative_args(mod_message["external_id"])))
    else:
        print("WARN", "Requested external id note does not exist:", mod_message["external_id"])

# Abstraction of the SC s_new message with id-increments, garbage collection and boilerplate args  
# Each dict[any,any] will contain a target var and a list of "args" vars that are used to construct the needed OSC
def s_new(data_arr: list[dict[any, any]]):

    for data in data_arr:

        amp = data["args"]["amp"] if "amp" in data["args"] else 0.0

        if amp > 0.0:

            # Mono test - find latest same source synth and reuse
            # TODO: Port to function or object, rethink the split("_") part 
            mono = data["args"]["mono"] if "mono" in data["args"] else 0.0
            matches = [key for key in external_id_map if key.split("_")[0] == data["source"]]
            mono_sent = False
            if matches and mono > 0.0:
                existing_note = external_id_map[matches[-1]]
                if existing_note.is_active(datetime.now()):
                    n_set(matches[-1], data["args"])
                    # Re-init to reset autogate etc. 
                    external_id_map[matches[-1]] = RunningNote(existing_note.nodeId, data)
                    mono_sent = True
            if not mono_sent:
                new_note = RunningNote(incrementer.next(), data)
                external_id = data["source"] + "_" + str(int(new_note.nodeId))
                external_id_map[external_id] = new_note
                sc.send_to_server("/s_new", new_note.to_OSC(get_relative_args(external_id)))
        
    return "OK"

# Generate a wav file from the note array (as if played in order)
# See nrt_record.py
# Expects a body of List[Dict] (or json array) note objects
def nrt_record(file_name: str, bpm: int, data_arr: list[dict[any, any]], is_sample = False):

    rows = []
    nrt_rows = []

    for emsg in effect_messages:
        nrt_rows.append([0.0, emsg])

    if is_sample:
        nrt_rows += buffer_data.to_buffer_nrt_defs()

    for data in data_arr:
        note = RunningNote(incrementer.next(), data)
        if is_sample:
            rows.append(note.to_sample_OSC(buffer_sorted, buffer_data))
        else:
            rows.append(note.to_OSC())

    script_data = build_nrt_script(rows, nrt_rows, bpm, file_name)
    with open("tmp/nrt_batch.scd", "w") as output_file:
        output_file.write(script_data)
        sc.send_to_client("/customscript", ["tmp/nrt_batch.scd"])


# Play a numbered sample synth.
# Uses s_new to trigger a synth object, typically linked to a buffer matching 
#   the ordinal placement of the particular sample .wav in the sample_name sample_packs subfolder.
# Like with s_new, a json payload with custom osc variables can be provided. 
#  
# See samples.py.
def trigger_sample(data_arr: list[dict[any, any]]):

    for data in data_arr:
        new_note = RunningNote(incrementer.next(), data)

        if len(new_note.to_sample_OSC(buffer_sorted, buffer_data)) > 0:

            sc.send_to_server("/s_new", new_note.to_sample_OSC(buffer_sorted, buffer_data))
            external_id = data["target"] + "_" + str(new_note.nodeId)
            external_id_map[external_id] = new_note
            
    return "OK"


# Init messaging system
context = zmq.Context()
subscription = context.socket(zmq.SUB)
#subscription.setsockopt(zmq.RCVHWM, 2) # Drop new messages if more than 2 are processing 
subscription.connect("tcp://localhost:5560")
subscription.setsockopt(zmq.SUBSCRIBE, b'JDW.PLAY.NOTE')
subscription.setsockopt(zmq.SUBSCRIBE, b'JDW.PLAY.SAMPLE')
subscription.setsockopt(zmq.SUBSCRIBE, b'JDW.ADD')
subscription.setsockopt(zmq.SUBSCRIBE, b'JDW.NSET')
subscription.setsockopt(zmq.SUBSCRIBE, b'JDW.MOD')
subscription.setsockopt(zmq.SUBSCRIBE, b'JDW.PROSC')
subscription.setsockopt(zmq.SUBSCRIBE, b'JDW.REMOVE.EFFECTS')
subscription.setsockopt(zmq.SUBSCRIBE, b'JDW.SEQ.BATCH')

print("Subscribed to tcp://localhost:5560")

# Run some notes in another thread to notify the user that the system is live 
subprocess.Popen(args=["python", "playtest.py"])

record_send_time = 0

poller = zmq.Poller()
poller.register(subscription)


class Message:
    def __init__(self, json_contents, msg_time_str, msg_type):
        self.json_contents = json_contents
        self.msg_time_str = msg_time_str
        self.msg_type = msg_type

def get_message(incoming) -> Message:
    message = incoming 
    before_read = datetime.now(timezone.utc)
    message_decoded = message.decode('utf-8')


    def scan_header(my_message):
        header = ""
        for c in my_message:
            if c != "[" and c != "{":
                header += c
            else:
                break
        return header 

    full_header = scan_header(message_decoded)

    header_parts = full_header.split("::")

    print(header_parts)
    msg_type = header_parts[0]
    msg_time_str = header_parts[1] if len(header_parts) > 1 else ""
    post_header = message_decoded.split(full_header)
    msg_json = post_header[1] if len(post_header) > 1 else ""
    msg_json = msg_json if "" != msg_json else "{}"


    #print("msg of type: ", msg_type, "sent at time", msg_time_str)

    timestamp = dateparse(msg_time_str) if msg_time_str else datetime.now(timezone.utc)
    print(
        "Msg took these microseconds to send: ",
            (datetime.now(timezone.utc) - timestamp).microseconds,
            "With this deparsing time: ",
            (datetime.now(timezone.utc) - before_read).microseconds
    )
    return Message(msg_json, msg_time_str, msg_type)

def process_message(message: Message):
    global record_send_time

    msg_time_str = message.msg_time_str
    msg_json = message.json_contents
    msg_type = message.msg_type

    print("msg of type: ", msg_type, "sent at time", msg_time_str)

    timestamp = dateparse(msg_time_str) if msg_time_str else datetime.now(timezone.utc)
    #print("Msg took these microseconds to send: ", (datetime.now(timezone.utc) - timestamp).microseconds)

    if msg_type == "JDW.PLAY.NOTE":
        if msg_json != "{}":
            s_new([json.loads(msg_json)])

    elif msg_type == "JDW.PLAY.SAMPLE":
        if msg_json != "{}":
            trigger_sample([json.loads(msg_json)])

    elif msg_type == "JDW.SEQ.BATCH":
        # Input is a set of messages
        samples = [json.loads("".join(msg.split("::")[1:])) for msg in json.loads(msg_json) if "::" in msg and msg.split("::")[0] == "JDW.PLAY.SAMPLE"]
        notes = [json.loads("".join(msg.split("::")[1:])) for msg in json.loads(msg_json) if "::" in msg and msg.split("::")[0] == "JDW.PLAY.NOTE"]
        if samples:
            #print("samples", samples)
            trigger_sample(samples)
        if notes:
            #print("notes", notes)
            s_new(notes)
            
            #print("Sample done playing at time", datetime.now())
    elif msg_type == "JDW.REMOVE.EFFECTS":
        kill_effects()
    elif msg_type == "JDW.ADD.EFFECT":
        print("############## INCOMING EFFECT!")
        if msg_json != "{}":
            add_effect(json.loads(msg_json))
    elif msg_type == "JDW.PROSC.SYNDEF.UPDATE":
        update()
    elif msg_type == "JDW.PROSC.NRT.RECORD":
        
        nrt_msg = json.loads(msg_json)
        nrt_record(nrt_msg["filename"], nrt_msg["bpm"], nrt_msg["payload"], nrt_msg["type"] == "sample")
    elif msg_type == "JDW.ADD.NOTE":
        nrt_msg = json.loads(msg_json)
        new_note(nrt_msg)
    elif msg_type == "JDW.MOD.NOTE":
        nrt_msg = json.loads(msg_json)
        modify_note(nrt_msg)
    elif msg_type == "JDW.NSET.NOTE":
        nrt_msg = json.loads(msg_json)
        n_set(nrt_msg["external_id"], nrt_msg["args"])
    else:
        print("############ UNKNOWN MESSAGE", msg_type)

    process_time = (datetime.now(timezone.utc) - timestamp).microseconds

    record_send_time = process_time
    #if process_time > 11000:
    #print("Warning! Long process time (>500 miS): ", record_send_time, "for message", message)

while True:

    try:

        #print("Processing...")
        polled = dict(poller.poll(0)) # 0ms - might need 1ms to actually time out  

        #print(polled)
        if subscription in polled:
            process_message(get_message(subscription.recv()))
        else:
            time.sleep(0.001)

        send_off_notes()

    except:
        print(sys.exc_info())
        print('!: Error or keyboard interrupt, exiting ...')

        for key in external_id_map:
            note = external_id_map[key]
            sc.send_to_server("/n_free", [note.nodeId])

        sc.kill()

        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
