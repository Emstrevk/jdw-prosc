from __future__ import annotations # Not needed after python 3.10
import os

# Ready a custom script that reads all samples into numbered buffers
def recompile() -> BufferData:

    buffer_data = get_buffer_data()
    with open("custom_scripts/buffer_load.scd", "w") as output_file:
        output_file.write(buffer_data.to_buffer_script())

    return buffer_data

def get_buffer_data() -> BufferData:
    subfolders = [ f.path for f in os.scandir("sample_packs") if f.is_dir() ]

    buffer_data = BufferData()

    for subdir in subfolders:
        sample_pack = SamplePack(subdir)

        # Wav files in dir sorted by name 
        dir_files = sorted([f for f in os.listdir(subdir) if os.path.isfile(os.path.join(subdir, f)) and ".wav" in str(f).lower()])

        # Assign next buffer number to the file sample
        for dir_f in dir_files:
            sample_pack.samples.append(BufferSample(os.path.join(subdir, dir_f), buffer_data.next_index()))
    
        # Base name of sample pack dir becomes the sample pack name/key
        buffer_data.samples[os.path.basename(subdir)] = sample_pack

    return buffer_data

class BufferData:
    def __init__(self):
        self.samples: dict[str, SamplePack] = {}
        self.current_max_buffer_index = 0
    
    def next_index(self):
        self.current_max_buffer_index += 1
        return self.current_max_buffer_index

    def get_sample(self, pack_name: str, sample_number: int) -> BufferSample:
        if not pack_name in self.samples:
            print("WARN: no sample pack named", pack_name)
            return None
        if len(self.samples[pack_name].samples) < sample_number:
            print("WARN: sample number", sample_number, "out of range for pack", pack_name)
            return None

        return self.samples[pack_name].samples[sample_number]

    def to_buffnum(self, pack_name: str, sample_number: int) -> int:
        sample = self.get_sample(pack_name, sample_number)
        if sample:
            return sample.buffer_number
        else:
            return 0

    # Regular scd way of reading everything to buffer
    def to_buffer_script(self) -> str:

        line_template = """Buffer.read(s, File.getcwd +/+ "{WAV_PATH}", 0, -1, bufnum: {BUFFER_NUMBER});"""

        script_data = ""

        for pack_name in self.samples:
            for sample in self.samples[pack_name].samples:
                line = line_template.replace("{WAV_PATH}", sample.file_path) \
                    .replace("{BUFFER_NUMBER}", str(sample.buffer_number))

                script_data += line 
                script_data += "\n"

        return script_data
        
    # https://doc.sccode.org/Guides/Non-Realtime-Synthesis.html
    # Reading buffers in NRT requires creating buffer objects to handle the reading 
    def to_buffer_nrt_defs(self) -> list[str]:
        template = "[0.0, (Buffer.new(server, 44100 * 8.0, 2, bufnum: {BUFNUM})).allocReadMsg(File.getcwd +/+ \"{PATH}\")]"
        
        rows = []
        for pack_name in self.samples:
            for sample in self.samples[pack_name].samples:
                rows.append(template.replace("{BUFNUM}", str(sample.buffer_number)).replace("{PATH}", sample.file_path))
        return rows
        

class SamplePack:
    def __init__(self, path: str):
        self.path = path

        # Expected to implicitly be in name order
        self.samples: list[BufferSample] = []

class BufferSample:
    def __init__(self, file_path: str, buffer_number: int):
        self.file_path = file_path
        self.buffer_number = buffer_number

# Basic auto-incrementer. Can exceed max buffer limit of supercollider if not careful... 
class BufferIndex:
    def __init__(self):
        self.current = 0

    def next(self):
        self.current += 1
        return self.current
